using System;
using Abp.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace dotnet_mvc_areas_stack.Areas.Admin.Controllers
{
    [Area("Admin")]

    public class BankHolidaysController : AbpController
    {

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Create()
        {
            Console.WriteLine("Creating");
            return View();
        }
    }
}
